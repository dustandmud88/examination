package utiles;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import utiles.UtilitariosWebElement;
import utiles.UtilitariosButton;

import com.google.common.base.Function;

public class UtilitariosComboBox {

	//Seleccionar una opción del combo con el valor indicado sin JS 
	public static void cmbSeleccionarUnaOpcionConClic (String propertyName, String valor, final List<WebElement> opcionesCmb, WebDriver driver, ITestResult testResult ){
			
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
		wait.pollingEvery(Constants.tiempo_milisegundosIntentarCada,  TimeUnit.MILLISECONDS);
		wait.withTimeout(Constants.tiempo_segundosHastIntervalo, TimeUnit.MINUTES).until(new Function<WebDriver, Boolean>() {
			public Boolean apply(WebDriver driver ) {				
				if(opcionesCmb.size() >1) {
					return true;
				}else {
					return false;
				}
			}
        });
		
		WebElement seleccion; 
		int tamanio = opcionesCmb.size();
		
		try {
			for (int i=0; i< tamanio;i++){
				
				seleccion = opcionesCmb.get(i);
		    	JavascriptExecutor executor = (JavascriptExecutor)driver; 
		    	executor.executeScript("arguments[0].scrollIntoView();", seleccion);
		    	
				if (seleccion.getAttribute(propertyName).compareTo(valor)==0){						
					i= tamanio;
					new WebDriverWait(driver, Constants.tiempo_segundosEspera).until(ExpectedConditions.elementToBeClickable(seleccion));
					UtilitariosButton.btn_clic(seleccion, driver, testResult);		
					break;
				}
			}		
		}catch (Exception e){
			UtilitariosReporte.stackTracePrintException(e,testResult);
		}
		
		
	}
	public static void cmbSeleccionarUnaOpcionClicBuscarXTexto (String texto,  final List<WebElement> opcionesCmb, WebDriver driver, ITestResult testResult ){
		
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
		wait.pollingEvery(Constants.tiempo_milisegundosIntentarCada,  TimeUnit.MILLISECONDS);
		wait.withTimeout(Constants.tiempo_segundosHastIntervalo, TimeUnit.MINUTES).until(new Function<WebDriver, Boolean>() {
			public Boolean apply(WebDriver driver ) {				
				if(opcionesCmb.size() >1) {
					return true;
				}else {
					return false;
				}
			}
        });
		
		WebElement seleccion; 
		int tamanio = opcionesCmb.size();
		
		try {
			for (int i=0; i< tamanio;i++){
				
				seleccion = opcionesCmb.get(i);
		    	JavascriptExecutor executor = (JavascriptExecutor)driver; 
		    	executor.executeScript("arguments[0].scrollIntoView();", seleccion);
		    	
				if (seleccion.getText().contains(texto)){						
					i= tamanio;
					new WebDriverWait(driver, Constants.tiempo_segundosEspera).until(ExpectedConditions.elementToBeClickable(seleccion));
					UtilitariosButton.btn_clic(seleccion, driver, testResult);	
				}
			}		
		}catch (Exception e){
			UtilitariosReporte.stackTracePrintException(e,testResult);
		}
		
		
	}
	//Seleccionar una opción del combo con el valor indicado con JS
	public static void cmbSeleccionarUnaOpcionConClicJS (String propertyName, String valor, final List<WebElement> opcionesCmb, WebDriver driver, ITestResult testResult ){
		
		 
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
		wait.pollingEvery(250,  TimeUnit.MILLISECONDS);
		wait.withTimeout(1, TimeUnit.MINUTES).until(new Function<WebDriver, Boolean>() {
			public Boolean apply(WebDriver driver ) {				
				if(opcionesCmb.size() >1) {
					return true;
				}else {
					return false;
				}
			}
        });
		
		WebElement seleccion; 
		int tamanio = opcionesCmb.size();
		
		try {
			for (int i=0; i< tamanio;i++){
				
				seleccion = opcionesCmb.get(i);
				if (seleccion.getAttribute(propertyName).compareTo(valor)==0){						
					i= tamanio;
					UtilitariosWebElement.escroleaHastaWebElement(seleccion, driver);
					new WebDriverWait(driver, Constants.tiempo_segundosEspera).until(ExpectedConditions.elementToBeClickable(seleccion));
					UtilitariosButton.btn_clicConJS(seleccion, driver, testResult);
				}
			}		
		}catch (Exception e){
			UtilitariosReporte.stackTracePrintException(e,testResult);
		}
		
		
	}
	
	//devolver un WebElemento del valorBuscado de un listado de WebElement
	public static WebElement devolverWebElementPorValor (String parametroBusqueda, String valorBusqueda,List<WebElement>listadoOpciones,  ITestResult testResult ){
		
		WebElement seleccion ;

		int tamanio = listadoOpciones.size();
		
		try {
			for (int i=0; i< tamanio;i++){
				seleccion = listadoOpciones.get(i);
				if (seleccion.getAttribute(parametroBusqueda).compareTo(valorBusqueda)==0){
					return seleccion;			
				} 
			}		
		}catch (Exception e){
			UtilitariosReporte.stackTracePrintException(e,testResult);
		}
		return null;
		
	}
	

	
}
