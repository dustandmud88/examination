package utiles;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;

public class UtilitariosButton {
	//submit que tenga este evento.
	public static void btn_submit (WebElement button, WebDriver driver, ITestResult testResult) {
		new WebDriverWait(driver, Constants.tiempo_segundosEspera).until(ExpectedConditions.visibilityOf(button));
		try {
			button.submit();
		}catch (Exception e){
			UtilitariosReporte.stackTracePrintException(e,testResult);
		}
	}
	//hacer clic al botón
	public static void btn_clic(WebElement button, WebDriver driver, ITestResult testResult) {
		new WebDriverWait(driver, Constants.tiempo_segundosEspera).until(ExpectedConditions.visibilityOf(button));
		try {
			button.click();	
		}catch (Exception e){
			UtilitariosReporte.stackTracePrintException(e,testResult);
		}
	}
	//hacer clic al botón con JS
	public static void btn_clicConJS(WebElement button, WebDriver driver, ITestResult testResult){
		new WebDriverWait(driver, Constants.tiempo_segundosEspera).until(ExpectedConditions.visibilityOf(button));

		try {
			JavascriptExecutor executor = (JavascriptExecutor)driver; 
			executor.executeScript("arguments[0].click();", button); 
		}catch (Exception e){
			UtilitariosReporte.stackTracePrintException(e,testResult);
		}
	} 
	//hacer clic al botón con JS
	public static void btn_clicConJSIsClickable(WebElement button, WebDriver driver, ITestResult testResult){
		
		new WebDriverWait(driver, Constants.tiempo_segundosEspera).until(ExpectedConditions.elementToBeClickable(button));
		
		try {
			JavascriptExecutor executor = (JavascriptExecutor)driver; 
			executor.executeScript("arguments[0].click();", button); 
		}catch (Exception e){
			UtilitariosReporte.stackTracePrintException(e,testResult);
		}
	} 
	
	public static void btn_submitConJSIsClickable(WebElement button, WebDriver driver, ITestResult testResult){
		new WebDriverWait(driver, Constants.tiempo_segundosEspera).until(ExpectedConditions.elementToBeClickable(button));

		try {
			JavascriptExecutor executor = (JavascriptExecutor)driver; 
			executor.executeScript("arguments[0].submit();", button); 
		}catch (Exception e){
			UtilitariosReporte.stackTracePrintException(e,testResult);
		}
	} 
	
	public static void btn_subirArchivo (WebElement button,String path, WebDriver driver, ITestResult testResult) {
		new WebDriverWait(driver, Constants.tiempo_segundosEspera).until(ExpectedConditions.visibilityOf(button));
		try {
			button.sendKeys(path);
		}catch (Exception e){
			UtilitariosReporte.stackTracePrintException(e,testResult);
		}
	}
	public static void btn_subirArchivoSinWait (WebElement button,String path, WebDriver driver, ITestResult testResult) {
		//new WebDriverWait(driver, Constants.tiempo_segundosEspera).until(ExpectedConditions.elementToBeSelected(button));
		try {
			button.sendKeys(path);
		}catch (Exception e){
			UtilitariosReporte.stackTracePrintException(e,testResult);
		}
	}
	
}
