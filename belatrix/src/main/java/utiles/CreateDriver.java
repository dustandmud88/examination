package utiles;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * @author Carl Cocchiaro
 *
 * Selenium Driver Class
 *
 */
public class CreateDriver {
    // local variables
    public static CreateDriver instance = null;
    private static final int IMPLICIT_TIMEOUT = 10;
    private ThreadLocal<WebDriver> webDriver = new ThreadLocal<WebDriver>();
    private ThreadLocal<String> sessionId = new ThreadLocal<String>();
    private ThreadLocal<String> sessionBrowser = new ThreadLocal<String>();
    private ThreadLocal<String> sessionPlatform = new ThreadLocal<String>();
    private ThreadLocal<String> sessionVersion = new ThreadLocal<String>();
  

    // constructor
    public CreateDriver() {
    	
    }

    /**
     * getInstance method to retrieve active driver instance
     *
     * @return CreateDriver
     */

    public static CreateDriver getInstance() {
        if ( instance == null ) {
            instance = new CreateDriver();
        }

        return instance;
    }

    /**
     * setDriver method to create driver instance
     *
     * @param browser
     * @param environment
     * @param platform
     * @param optPreferences
     * @throws Exception
     */
    @SafeVarargs
    
    public final void setDriver(String browser,
                                Map<String, Object>... optPreferences)
                                throws Exception {



	    
        if(browser.compareTo("firefox" ) == 0) {
        	
		    	  
		    	  WebDriverManager.firefoxdriver().setup();
	        	  webDriver.set(new FirefoxDriver());
	        	  
        }else {
        	 if(browser.compareTo("chrome" ) == 0) {
        		 
		    	   
		    	     WebDriverManager.chromedriver().setup();
		    		 webDriver.set(new ChromeDriver());
	                 	              
        	 }else {
        		 if(browser.compareTo("internet explorer" ) == 0) {
        			     WebDriverManager.iedriver().arch32();        
		    	         WebDriverManager.iedriver().setup();
		    	         	    	                  
		                 webDriver.set(new InternetExplorerDriver());   
		                                
		         }else {
			          if(browser.compareTo("edge")==0) {
			        	  
				    	  WebDriverManager.edgedriver().setup();
				          webDriver.set(new EdgeDriver());    
				          
		        	 }else {
		        		  if(browser.compareTo("opera")==0) {
					    	  WebDriverManager.operadriver().setup();
					          webDriver.set(new OperaDriver());    
		        		  }
		        	 }
		         }	 
        	}	 
        }
 

        sessionId.set(((RemoteWebDriver) webDriver.get()).getSessionId().toString());

        System.out.println("\n*** TEST ENVIRONMENT = "
                + "/Session ID=" + getSessionId() + "\n");
        
        getDriver().manage().timeouts().implicitlyWait(IMPLICIT_TIMEOUT, TimeUnit.SECONDS);
        getDriver().manage().window().maximize();
        
    }

    /**
     * getDriver method to retrieve active driver
     *
     * @return WebDriver
     */
    public WebDriver getDriver() {
        return webDriver.get();
    }

    /**
     * closeDriver method to close active driver
     *
     */
    public void closeDriver() {
        try {
            getDriver().quit();
            webDriver.get().close();
        }

        catch ( Exception e ) {
            // do something
        }
    }

    /**
     * getSessionId method to retrieve active id
     *
     * @return String
     * @throws Exception
     */
    public String getSessionId() throws Exception {
        return sessionId.get();
    }

    /**
     * getSessionBrowser method to retrieve active browser
     * @return String
     * @throws Exception
     */
    public String getSessionBrowser() throws Exception{
        return sessionBrowser.get();
    }

    /**
     * getSessionVersion method to retrieve active version
     *
     * @return String
     * @throws Exception
     */
    public String getSessionVersion() throws Exception {
        return sessionVersion.get();
    }

    /**
     * getSessionPlatform method to retrieve active platform
     * @return String
     * @throws Exception
     */
    public String getSessionPlatform() throws Exception {
        return sessionPlatform.get();
        
    }

}