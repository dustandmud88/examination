package pe.com.belatrix;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import org.testng.ITestResult;


import utiles.UtilitariosButton;
import utiles.UtilitariosTextField;

public class MainPage {
	
	
	WebDriver driver;
	
	@FindBy	(id = "gh-ac")
	WebElement txtBox_Search;
	
	@FindBy (id = "gh-btn")
	WebElement btn_Search;
	
	public MainPage (WebDriver driver) {
		  this.driver = driver;
		  PageFactory.initElements(driver, this);  	  
	
	}
	
	public void searchForText(String searchText, ITestResult testResult) {
		UtilitariosTextField.txtField_clean(txtBox_Search, driver, testResult);
		UtilitariosTextField.txtField_putStringJS(searchText, txtBox_Search, driver, testResult);
		UtilitariosButton.btn_clicConJSIsClickable(btn_Search, driver, testResult);
	}
	
}
